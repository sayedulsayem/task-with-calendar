<form action="{{ route('tasks.store') }}" method="post">
    {{ csrf_field() }}
    Task name:
    <br />
    <input type="text" name="name" />
    <br /><br />
    Task description:
    <br />
    <textarea name="description"></textarea>
    <br /><br />
    Start date:
    <br />
    <input type="date" name="starting_date" class="date" />
    <br /><br>
    Ending date:
    <br/>
    <input type="date" name="ending_date" class="date" />
    <br /><br>
    Starting Time:
    <br>
    <input type="time" name="starting_time" class="date" />
    <br /><br>
    Ending Time:
    <br>
    <input type="time" name="ending_time" class="date" />
    <br /><br />
    <input type="submit" value="Save" />
</form>

<script src="https://code.jquery.com/jquery-1.11.3.min.js"></script>
<script src="https://code.jquery.com/ui/1.11.3/jquery-ui.min.js"></script>
